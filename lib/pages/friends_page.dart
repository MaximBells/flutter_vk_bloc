import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vk_bloc/bloc/event.dart';
import 'package:vk_bloc/bloc/vk_bloc.dart';
import 'package:vk_bloc/model/user.dart';

class FriendPage extends StatelessWidget {
  List<dynamic> loadedFriend;
  String link;

  FriendPage(List<dynamic> _loadedFriend, String _link) {
    loadedFriend = _loadedFriend;
    link = _link;
  }
  Future<void> _showMyDialog(
      BuildContext context, Friend friend, VkBloc vkBloc) async {
    List<ChatProfiles> profiles = [];
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('${friend.first_name} ${friend.last_name} '),
          content: Container(
            height: 200,
            child: Center(
              child: Stack(
                alignment: Alignment.bottomRight,
                children: [
                  CircleAvatar(
                      radius: 90,
                      backgroundImage: NetworkImage(friend.photo_200)),
                  friend.online_mobile == 1
                      ? Positioned(
                          bottom: 3.0,
                          right: 3.0,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Stack(children: <Widget>[
                                Positioned.fill(
                                  child: Container(
                                    margin: EdgeInsets.all(12),
                                    color: Colors.white, // Color
                                  ),
                                ),
                                Icon(
                                  Icons.phone_android,
                                  color: Colors.grey[50],
                                  size: 55.0,
                                ),
                              ]),
                              Icon(
                                Icons.phone_android,
                                color: Colors.green,
                                size: 45.0,
                              ),
                            ],
                          ),
                        )
                      : Positioned(
                          bottom: 5.0,
                          right: 5.0,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Icon(
                                Icons.circle,
                                color: Colors.grey[50],
                                size: 50.0,
                              ),
                              Icon(
                                Icons.circle,
                                color: friend.online == 1
                                    ? Colors.green
                                    : Colors.grey,
                                size: 40.0,
                              ),
                            ],
                          ),
                        ),
                ],
              ),
            ),
          ),
          // contentPadding: EdgeInsets.all(70.0),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Закрыть'),
            ),
            // ignore: deprecated_member_use
            FlatButton(
                onPressed: () {
                  vkBloc.add(VkDialogPage(
                      link,
                      friend.id,
                      '${friend.first_name} ${friend.last_name}',
                      friend.photo_100,
                      profiles,
                      'user'));
                  Navigator.pop(context);
                },
                child: Text('Написать')),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final VkBloc vkBloc = BlocProvider.of<VkBloc>(context);
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              vkBloc.add(VkLoadPage(link));
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          title: Text('Мои друзья'),
          backgroundColor: Colors.blue,
        ),
        body: new ListView.separated(
          physics: BouncingScrollPhysics(),
          separatorBuilder: (context, index) => Divider(
            color: Colors.grey[50],
            height: 8,
          ),
          itemCount: loadedFriend.length,
          padding:
              const EdgeInsets.only(right: 8.0, left: 8.0, top: 4, bottom: 4),
          itemBuilder: (context, index) {
            return GestureDetector(
                child: Container(
                  width: MediaQuery.of(context).size.width - 16,
                  color: Colors.transparent,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: CircleAvatar(
                          radius: 25,
                          backgroundImage:
                              NetworkImage(loadedFriend[index].photo_100),
                        ),
                      ),
                      Text(
                          '${loadedFriend[index].first_name} ${loadedFriend[index].last_name}',
                          style: new TextStyle(fontSize: 15.0),
                          overflow: TextOverflow.ellipsis),
                      SizedBox(
                        width: 16,
                      ),
                      loadedFriend[index].online_mobile == 1
                          ? Icon(
                              Icons.phone_android,
                              color: Colors.green,
                              size: 15.0,
                            )
                          : Icon(
                              Icons.circle,
                              color: loadedFriend[index].online == 1
                                  ? Colors.green
                                  : Colors.grey,
                              size: 10.0,
                            ),
                    ],
                  ),
                ),
                onTap: () {
                  _showMyDialog(context, loadedFriend[index], vkBloc);
                });
          },
        ),
      ),
      onWillPop: () async {
        //Замена события
        vkBloc.add(VkLoadPage(link));
        return false;
      },
    );
  }
}
