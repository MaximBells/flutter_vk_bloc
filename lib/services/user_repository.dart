import 'package:vk_bloc/model/user.dart';
import 'package:vk_bloc/services/user_api_provider.dart';

class UserRepository {
  UserProvider _userProvider = UserProvider();

  //Future<List<User>>getAllUsers() => _userProvider.getUser(link);
  Future<List<User>> getAllUsers(String _link) {
    return _userProvider.getUser(_link);
  }
}

class FriendRepository {
  FriendProvider _friendProvider = FriendProvider();

  Future<List<Friend>> getAllFriend(String _link) {
    return _friendProvider.getFriend(_link);
  }
}

class ConversationRepository {
  ChatProvider _chatProvider = ChatProvider();
  ConversationIdProvider _conversationIdProvider = ConversationIdProvider();
  Future<List<ChatUsers>> chatUsers;

  Future<List<ConversationsId>> getAllConversation(String _link) {
    return _conversationIdProvider.getConversation(_link);
  }

  Future<List<ChatProfiles>> getAllChatProfiles(String _link) {
    return _chatProvider.getChatProfiles(_link);
  }

  String getName(List<ChatProfiles> chatProfiles, int id) {
    return _chatProvider.getName(chatProfiles, id);
  }

  String getPhoto(List<ChatProfiles> chatProfiles, int id) {
    return _chatProvider.getPhoto(chatProfiles, id);
  }
}

class ChatFullRepository {
  final conversationRepository = ConversationRepository();
  List<ChatUsers> chatUsers = [];

  List<ChatUsers> getAllChats(List<ConversationsId> loadedConversationsId,
      List<ChatProfiles> loadedChatProfiles) {
    loadedConversationsId.forEach((element) {
      if (element.type == 'chat') {
        chatUsers.add(ChatUsers(
            element.id,
            element.title,
            element.messageText,
            element.photo_100,
            element.time.toString(),
            element.unread_count == null ? false : true,
            element.type,
            element.in_read == element.out_read ? false : true));
      } else {
        chatUsers.add(ChatUsers(
            element.id,
            conversationRepository.getName(loadedChatProfiles, element.id),
            element.messageText,
            conversationRepository.getPhoto(loadedChatProfiles, element.id),
            element.time.toString(),
            element.unread_count == null ? false : true,
            element.type,
            element.in_read == element.out_read ? false : true));
      }
    });
    return chatUsers;
  }
}

class MessageRepository {
  final messageProvider = MessageProvider();

  Future<List<Message>> getAllMessage(String link, int peer_id) {
    return messageProvider.getMessage(link, peer_id);
  }

  Future<int> sendAllMessage(String link, int peer_id, String message) {
    return messageProvider.sendMessage(link, peer_id, message);
  }
}
