class User {
  int id;

  // ignore: non_constant_identifier_names
  String first_name;

  // ignore: non_constant_identifier_names
  String last_name;
  String photo_200;
  int online;
  int online_mobile = 0;
  String status;

  // ignore: non_constant_identifier_names
  User(
      {this.id,
      this.first_name,
      this.last_name,
      this.photo_200,
      this.online,
      this.online_mobile,
      this.status});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      first_name: json['first_name'],
      last_name: json['last_name'],
      photo_200: json['photo_200'],
      online: json['online'],
      online_mobile: json['online_mobile'],
      status: json['status'],
    );
  }
}

class Message {
  int id;
  int from_id;
  String text;
  int date;

  Message({this.id, this.from_id, this.text, this.date});

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
        id: json['id'],
        text: json['text'],
        from_id: json['from_id'],
        date: json['date']);
  }
}

class Friend {
  String first_name;
  String last_name;
  String photo_100;
  String photo_200;
  int id;
  int online;
  int online_mobile = 0;

  Friend({
    this.first_name,
    this.last_name,
    this.photo_100,
    this.photo_200,
    this.id,
    this.online,
    this.online_mobile,
  });

  factory Friend.fromJson(Map<String, dynamic> json) {
    return Friend(
      id: json['id'],
      first_name: json['first_name'],
      last_name: json['last_name'],
      photo_100: json['photo_100'],
      photo_200: json['photo_200'],
      online: json['online'],
      online_mobile: json['online_mobile'],
    );
  }
}

class LinkGet {
  String link;

  void setLink(String _link) {
    link = _link;
  }

  String getLink() => link;
}

class AccessToken {
  String _access_token;
  String _user_id;
  void setAccessToken(String _link) {
    var uri = Uri.parse(_link);
    _access_token = uri.fragment.split('&')[0];
    _access_token = _access_token.split('=')[1];
    _user_id = uri.fragment.split('&')[2];
    _user_id = _user_id.split('=')[1];
  }

  String getAccessToken() {
    return _access_token;
  }

  String getUserId() {
    return _user_id;
  }
}

class UserId {
  String access_token;
  String user_id;
  void setUserId(String _link) {
    var uri = Uri.parse(_link);
    access_token = uri.fragment.split('&')[0];
    access_token = access_token.split('=')[1];
    user_id = uri.fragment.split('&')[2];
    user_id = user_id.split('=')[1];
  }
}

class ChatUsers {
  int id;
  String name;
  String messageText;
  String imageURL;
  String time;
  String typeOfDialog;
  bool unreadChat;
  bool unreadMessage;

  ChatUsers(int _id, String _name, String _messageText, String _imageUrl,
      String _time, bool _unreadChat, String _typeOfDialog, bool _unreadMessage) {
    typeOfDialog = _typeOfDialog;
    id = _id;
    name = _name;
    messageText = _messageText;
    imageURL = _imageUrl;
    time = _time;
    unreadChat = _unreadChat;
    unreadMessage = _unreadMessage;
  }
}

class ChatProfiles {
  String first_name;
  String last_name;
  int id;
  String photo_100;

  ChatProfiles({this.first_name, this.last_name, this.id, this.photo_100});

  factory ChatProfiles.fromJson(Map<String, dynamic> json) {
    return ChatProfiles(
        id: json['id'],
        first_name: json['first_name'],
        last_name: json['last_name'],
        photo_100: json['photo_100']);
  }
}

class ConversationsId {
  String type;
  int id;
  String photo_100;
  String title;
  String messageText;
  int time;
  int unread_count;
  int in_read;
  int out_read;

  ConversationsId(String _type, int _id, String _photo_100, String _title,
      String _messageText, int _time, int _unread_count, int _in_read, int _out_read) {
    photo_100 = _photo_100;
    title = _title;
    type = _type;
    id = _id;
    messageText = _messageText;
    time = _time;
    unread_count = _unread_count;
    in_read = _in_read;
    out_read = _out_read;
  }
}
