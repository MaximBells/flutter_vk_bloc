import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vk_bloc/bloc/event.dart';
import 'package:vk_bloc/bloc/vk_bloc.dart';
import 'package:bubble/bubble.dart';
import 'package:vk_bloc/model/user.dart';
import 'package:vk_bloc/services/user_api_provider.dart';
import 'package:vk_bloc/services/user_repository.dart';
import 'package:vk_bloc/services/message_crypt.dart';

class DialogPage extends StatelessWidget {
  int offset = 0;
  List<Message> offsetHistory = [];
  final aesCrypt = AesCrypt();
  final salsa20Crypt = Salsa20Crypt();
  final fernetCrypt = FernetCrypt();
  String link = '';
  String keyOne;
  String type;
  final keyField = TextEditingController();
  final messageRepository = MessageRepository();
  final messageProvider = MessageProvider();
  final conversationRepository = ConversationRepository();
  List<Message> loadedMessages = [];
  String typeOfCrypt = 'none';
  String name;
  String imageUrl;
  String user_id;
  int peer_id;
  List<ChatProfiles> profiles;
  final ScrollController _controllerScroll = ScrollController();

  DialogPage(
      String _link,
      List<Message> _messages,
      String _name,
      String _imageUrl,
      String _user_id,
      int _peer_id,
      List<ChatProfiles> _profiles,
      String _type) {
    type = _type;
    profiles = _profiles;
    user_id = _user_id;
    name = _name;
    imageUrl = _imageUrl;
    link = _link;
    loadedMessages = _messages;
    peer_id = _peer_id;
  }

  _scrollListener() async {
    if (_controllerScroll.offset >=
            _controllerScroll.position.maxScrollExtent &&
        !_controllerScroll.position.outOfRange) {
      print('reach the bottom');
    }
    if (_controllerScroll.offset <=
            _controllerScroll.position.minScrollExtent &&
        !_controllerScroll.position.outOfRange) {
      offset += 20;
      List<Message> newOffsetHistory =
          await messageProvider.getOffsetMessage(link, peer_id, offset);
      offsetHistory.addAll(newOffsetHistory);
    }
  }

  Future<void> _showMyDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Выбор шифрования'),
          content: SingleChildScrollView(
            child: ListBody(children: <Widget>[
              ElevatedButton(
                onPressed: () {
                  typeOfCrypt = 'AES';
                  Navigator.of(context).pop();
                },
                child: Text('AES'),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.red.shade300),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  typeOfCrypt = 'Salsa20';
                  Navigator.of(context).pop();
                },
                child: Text('Salsa20'),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.green.shade300),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  typeOfCrypt = 'Fernet';
                  Navigator.of(context).pop();
                },
                child: Text('Fernet'),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blue.shade300),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  typeOfCrypt = 'none';
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Без шифрования',
                  style: TextStyle(color: Colors.black87),
                ),
                style: ElevatedButton.styleFrom(
                    side: BorderSide(
                      width: 1.0,
                      color: Colors.blue.shade300,
                    ),
                    primary: Colors.white),
              ),
              SizedBox(
                height: 6,
              ),
              TextField(
                  controller: keyField,
                  decoration: InputDecoration(
                    fillColor: Colors.deepPurple,
                    // suffixIcon: Icon(Icons.vpn_key_outlined),
                    border: OutlineInputBorder(),
                    labelText: 'Введите ключ',
                  )),
            ]),
          ),
        );
      },
    );
  }

  _moveDown() {
    // _controllerScroll.animateTo(_controllerScroll.position.maxScrollExtent,
    //     curve: Curves.linear, duration: Duration(milliseconds: 200));
    _controllerScroll.jumpTo(_controllerScroll.position.maxScrollExtent + 15);
  }

  Stream<List<Message>> _getMessages() async* {
    _moveDown();
    while (true) {
      await Future<void>.delayed(Duration(seconds: 1));
      List<Message> messages =
          await messageRepository.getAllMessage(link, peer_id);
      loadedMessages.clear();
      loadedMessages = messages;
      loadedMessages.addAll(offsetHistory);
      loadedMessages.forEach((element) async {
        if (element.text.startsWith('messageCrypt')) {
          if (typeOfCrypt == 'Salsa20' &&
              element.text.split('\$')[1] == 'Salsa20') {
            element.text = await salsa20Crypt.decrypt(keyField.text,
                element.text.split('\$')[2].replaceAll(' ', '+'));
          }
          if (typeOfCrypt == 'AES' && element.text.split('\$')[1] == 'AES') {
            element.text = await aesCrypt.decrypt(keyField.text,
                element.text.split('\$')[2].replaceAll(' ', '+'));
          }
          if (typeOfCrypt == 'Fernet' &&
              element.text.split('\$')[1] == 'Fernet') {
            element.text = await fernetCrypt.decrypt(keyField.text,
                element.text.split('\$')[2].replaceAll(' ', '+'));
          }
        }
      });
      yield loadedMessages;
    }
  }

  @override
  Widget build(BuildContext context) {
    final VkBloc vkBloc = BlocProvider.of<VkBloc>(context);
    _controllerScroll.addListener(_scrollListener);
    final TextEditingController _controller = TextEditingController();
    return WillPopScope(
      onWillPop: () async {
        //Замена события
        vkBloc.add(VkChatPage(link));
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              icon: Icon(
                Icons.settings,
                color: Colors.white,
              ),
              onPressed: () {
                _showMyDialog(context);
              },
            ),
          ],
          elevation: 0,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.blue,
          flexibleSpace: SafeArea(
            child: Container(
              padding: EdgeInsets.only(right: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    onPressed: () {
                      vkBloc.add(VkChatPage(link));
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  CircleAvatar(
                    backgroundImage: NetworkImage(imageUrl),
                    maxRadius: 20,
                  ),
                  SizedBox(
                    width: 12,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          name,
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            StreamBuilder(
                stream: _getMessages(),
                initialData: loadedMessages,
                builder: (context, snapshot) {
                  return ListView.builder(
                    controller: _controllerScroll,
                    itemCount: loadedMessages.length,
                    shrinkWrap: false,
                    padding: EdgeInsets.only(top: 10, bottom: 70),
                    itemBuilder: (context, index) {
                      return ConstrainedBox(
                        constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (type != 'user' &&
                                loadedMessages[
                                            loadedMessages.length - 1 - index]
                                        .from_id !=
                                    int.parse(user_id))
                              Padding(
                                padding: const EdgeInsets.only(
                                    right: 4.0, left: 4.0, bottom: 4.0),
                                child: CircleAvatar(
                                  radius: 25,
                                  backgroundImage: NetworkImage(
                                    conversationRepository.getPhoto(
                                        profiles,
                                        loadedMessages[loadedMessages.length -
                                                1 -
                                                index]
                                            .from_id),
                                  ),
                                ),
                              ),
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth: type != 'user' &&
                                        loadedMessages[loadedMessages.length -
                                                    1 -
                                                    index]
                                                .from_id !=
                                            int.parse(user_id)
                                    ? MediaQuery.of(context).size.width - 68
                                    : MediaQuery.of(context).size.width - 4,
                              ),
                              child: Bubble(
                                padding: BubbleEdges.all(10.0),
                                margin: BubbleEdges.only(top: 15.0),
                                nip: loadedMessages[loadedMessages.length -
                                                1 -
                                                index]
                                            .from_id ==
                                        int.parse(user_id)
                                    ? BubbleNip.rightTop
                                    : BubbleNip.leftTop,
                                child: type == 'user'
                                    ? Text(loadedMessages[
                                            loadedMessages.length - 1 - index]
                                        .text)
                                    : Column(
                                        children: <Widget>[
                                          Text(loadedMessages[
                                                  loadedMessages.length -
                                                      1 -
                                                      index]
                                              .text)
                                        ],
                                      ),
                                alignment: loadedMessages[
                                                loadedMessages.length -
                                                    1 -
                                                    index]
                                            .from_id ==
                                        int.parse(user_id)
                                    ? Alignment.centerRight
                                    : Alignment.centerLeft,
                                color: loadedMessages[loadedMessages.length -
                                                1 -
                                                index]
                                            .from_id !=
                                        int.parse(user_id)
                                    ? Colors.grey.shade200
                                    : Colors.blue[200],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                }),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.only(left: 10, bottom: 10, top: 10),
                height: 60,
                width: double.infinity,
                color: Colors.white,
                child: Row(
                  children: <Widget>[
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: TextField(
                        controller: _controller,
                        decoration: InputDecoration(
                            hintText: "Начните писать...",
                            hintStyle: TextStyle(color: Colors.black54),
                            border: InputBorder.none),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    FloatingActionButton(
                      onPressed: () async {
                        if (_controller.text.isNotEmpty) {
                          if (typeOfCrypt == 'AES') {
                            String _encMessage = await aesCrypt.encrypt(
                                keyField.text, _controller.text, typeOfCrypt);
                            await messageProvider.sendMessage(
                                link, peer_id, _encMessage);
                            _controller.clear();
                          }
                          if (typeOfCrypt == 'Salsa20') {
                            String _encMessage = await salsa20Crypt.encrypt(
                                keyField.text, _controller.text, typeOfCrypt);
                            await messageProvider.sendMessage(
                                link, peer_id, _encMessage);
                            _controller.clear();
                          }
                          if (typeOfCrypt == 'Fernet') {
                            String _encMessage = await fernetCrypt.encrypt(
                                keyField.text, _controller.text, typeOfCrypt);
                            await messageProvider.sendMessage(
                                link, peer_id, _encMessage);
                            _controller.clear();
                          }
                          if (typeOfCrypt == 'none') {
                            await messageProvider.sendMessage(
                                link, peer_id, _controller.text);
                            _controller.clear();
                          }
                        }
                      },
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                        size: 25,
                      ),
                      backgroundColor: Colors.blue,
                      elevation: 0,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
