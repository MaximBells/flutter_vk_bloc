import 'package:vk_bloc/bloc/event.dart';
import 'package:vk_bloc/bloc/state.dart';
import 'package:vk_bloc/services/user_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vk_bloc/model/user.dart';

class VkBloc extends Bloc<VkEvent, VkState> {
  final UserRepository userRepository;
  final friendRepository = FriendRepository();
  final conversationRepository = ConversationRepository();
  final chatRepository = ChatFullRepository();
  final messageRepository = MessageRepository();

  VkBloc({this.userRepository})
      : assert(userRepository != null),
        super(null);

  @override
  Stream<VkState> mapEventToState(VkEvent event) async* {
    if (event is VkLoadPage) {
      yield VkLoadingState();
      try {
        final List<User> _loadedUserList =
            await userRepository.getAllUsers(event.getLink());
        yield VkLoadedState(loadedUser: _loadedUserList);
      } catch (_) {
        yield VkErrorState();
      }
    } else if (event is VkMainPage) {
      yield VkLoadedMainPageState();
    } else if (event is VkFriendsPage) {
      yield VkLoadingState();
      try {
        final List<Friend> _loadedFriendList =
            await friendRepository.getAllFriend(event.getLink());
        yield VkLoadedFriendState(loadedFriend: _loadedFriendList);
      } catch (_) {
        yield VkErrorState();
      }
    } else if (event is VkDialogPage) {
      yield VkLoadingState();
      try{final List<Message> _loadedMessageList = await messageRepository
          .getAllMessage(event.getLink(), event.getPeer());
      yield VkLoadedDialogState(
          loadedMessages: _loadedMessageList,
          name: event.getName(),
          imageUrl: event.getImageUrl(),
          peer_id: event.getPeer(),
          profiles: event.getProfiles(),
          type: event.getType());}catch(_){
        yield VkErrorState();
      }

    } else if (event is VkChatPage) {
      yield VkLoadingState();
      try{final List<ConversationsId> loadedConversationsId =
      await conversationRepository.getAllConversation(event.getLink());
      final List<ChatProfiles> loadedChatProfiles =
      await conversationRepository.getAllChatProfiles(event.getLink());
      List<ChatUsers> chatUsers =
      chatRepository.getAllChats(loadedConversationsId, loadedChatProfiles);
      yield VkLoadedChatState(
          loadedChat: chatUsers, loadedProfiles: loadedChatProfiles);}catch(_){
        yield VkErrorState();
      }

    }
  }
}

//final List<User> _loadedUserList = await userRepository.getAllUsers(userRepository.getLink());

/*@override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    if (event is UserLoadEvent) {
      yield UserLoadingState();
      try {
        final List<User> _loadedUserList = await userRepository.getAllUsers();
        yield UserLoadedState(loadedUser: _loadedUserList);
      } catch (_) {
        yield UserErrorState();
      }
    } else if (event is UserClearEvent) {
      yield UserEmptyState();
    }
  }
 */
