import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vk_bloc/bloc/event.dart';
import 'package:vk_bloc/bloc/vk_bloc.dart';
import 'package:vk_bloc/model/user.dart';
import 'package:vk_bloc/services/user_repository.dart';
import 'package:intl/intl.dart';

class Chats extends StatelessWidget {
  List<ChatUsers> chatUsers;

  String link;
  int offset = 0;
  final conversationRepository = ConversationRepository();
  final chatRepository = ChatFullRepository();

  List<ChatProfiles> profiles;

  Chats(
      String _link, List<ChatUsers> _chatUsers, List<ChatProfiles> _profiles) {
    profiles = _profiles;
    link = _link;
    chatUsers = _chatUsers;
  }

  Stream<List<ChatUsers>> _getChatUsers() async* {
    // This loop will run forever because _running is always true
    while (true) {
      await Future<void>.delayed(Duration(seconds: 1));
      List<ConversationsId> loadedConversationsId =
          await conversationRepository.getAllConversation(link);
      List<ChatProfiles> loadedChatProfiles =
          await conversationRepository.getAllChatProfiles(link);
      chatUsers.clear();
      var chats =
          chatRepository.getAllChats(loadedConversationsId, loadedChatProfiles);

      chatUsers = chats;
      yield chatUsers;
      // This will be displayed on the screen as current time
    }
  }

  @override
  Widget build(BuildContext context) {
    final VkBloc vkBloc = BlocProvider.of<VkBloc>(context);

    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: () {
                vkBloc.add(VkLoadPage(link));
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            title: Text('Мои cообщения'),
            backgroundColor: Colors.blue,
          ),
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                StreamBuilder(
                    initialData: chatUsers,
                    stream: _getChatUsers(),
                    builder: (context, snapshot) {
                      return ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: chatUsers.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return ConversationList(
                              chatUsers[index].name,
                              chatUsers[index].messageText,
                              chatUsers[index].imageURL,
                              '${DateFormat('kk:mm').format(DateTime.fromMillisecondsSinceEpoch(int.parse(chatUsers[index].time) * 1000))} ',
                              chatUsers[index].unreadChat,
                              link,
                              chatUsers[index].id,
                              profiles,
                              chatUsers[index].typeOfDialog,
                              chatUsers[index].unreadMessage);
                        },
                      );
                    })
              ],
            ),
          ),
        ),
        onWillPop: () async {
          vkBloc.add(VkLoadPage(link));
          return false;
        });
  }
}

class ConversationList extends StatelessWidget {
  String name = '';
  String messageText = '';
  String imageUrl = '';
  String time = '';
  bool isMessageRead = false;
  String link;
  int peer_id;
  List<ChatProfiles> profiles;
  String type;
  bool unreadMessage;

  ConversationList(
      String _name,
      String _messageText,
      String _imageUrl,
      String _time,
      bool _isMessageRead,
      String _link,
      int _peer_id,
      List<ChatProfiles> _profiles,
      String _type,
      bool _unreadMessage) {
    unreadMessage = _unreadMessage;
    type = _type;
    profiles = _profiles;
    name = _name;
    messageText = _messageText;
    imageUrl = _imageUrl;
    time = _time;
    isMessageRead = _isMessageRead;
    link = _link;
    peer_id = _peer_id;
  }

  @override
  Widget build(BuildContext context) {
    final VkBloc vkBloc = BlocProvider.of<VkBloc>(context);
    return GestureDetector(
      onTap: () {
        vkBloc.add(VkDialogPage(link, peer_id, name, imageUrl, profiles, type));
      },
      child: Container(
        padding: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: NetworkImage(imageUrl),
                    maxRadius: 30,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.transparent,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            name,
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Row(
                            children: [
                              if (unreadMessage && !isMessageRead)
                                Icon(
                                  Icons.circle,
                                  color: Colors.grey,
                                  size: 10.0,
                                ),
                              if (unreadMessage && !isMessageRead)
                                SizedBox(
                                  width: 4,
                                ),
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: isMessageRead
                                          ? Colors.grey.shade300
                                          : Colors.transparent,
                                      border: Border.all(
                                        color: isMessageRead
                                            ? Colors.grey.shade300
                                            : Colors.transparent,
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(3.0),
                                    child: Text(
                                      messageText,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 13,
                                          color: Colors.grey.shade700),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 16,
            ),
            Text(time,
                style: TextStyle(
                  fontSize: 12,
                )),
          ],
        ),
      ),
    );
  }
}
