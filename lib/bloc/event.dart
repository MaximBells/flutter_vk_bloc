abstract class VkEvent {}

class VkLoadPage extends VkEvent {
  String link = '';

  VkLoadPage(String _link) {
    link = _link;
  }

  String getLink() => link;
}

class VkFriendsPage extends VkEvent {
  String link = '';

  VkFriendsPage(String _link) {
    link = _link;
  }

  String getLink() => link;
}


class VkMainPage extends VkEvent {}

class VkDialogPage extends VkEvent {
  String link = '';
  int peer_id;
  String name;
  String imageUrl;
  List<dynamic>profiles;
  String type;
  VkDialogPage(String _link, int _peer_id, String _name, String _imageUrl, List<dynamic>_profiles, String _type) {
    type = _type;
    profiles = _profiles;
    name = _name;
    imageUrl = _imageUrl;
    link = _link;
    peer_id = _peer_id;
  }
  String getType()=>type;
  List<dynamic> getProfiles() => profiles;
  String getName() => name;
  String getImageUrl() => imageUrl;
  int getPeer() => peer_id;
  String getLink() => link;
}

class VkChatPage extends VkEvent{
  String link = '';
  VkChatPage(String _link) {
    link = _link;
  }
  String getLink() => link;
}