import 'package:flutter/foundation.dart';

abstract class VkState {}

class VkLoadingState extends VkState {}

class VkLoadedState extends VkState {
  List<dynamic> loadedUser;

  VkLoadedState({@required this.loadedUser}) : assert(loadedUser != null);
}

class VkLoadedMainPageState extends VkState {}

class VkLoadedFriendState extends VkState {
  List<dynamic> loadedFriend;

  VkLoadedFriendState({@required this.loadedFriend})
      : assert(loadedFriend != null);
}

class VkLoadedDialogState extends VkState {
  List<dynamic> loadedMessages;
  List<dynamic> profiles;
  String name;
  String imageUrl;
  int peer_id;
  String type;
  VkLoadedDialogState(
      {@required this.loadedMessages,
      @required this.name,
      @required this.imageUrl,
      @required this.peer_id,
      @required this.profiles,
      @required this.type})
      : assert(loadedMessages != null);
}

class VkLoadedChatState extends VkState {
  List<dynamic> loadedChat;
  List<dynamic> loadedProfiles;

  VkLoadedChatState({@required this.loadedChat, @required this.loadedProfiles})
      : assert(loadedChat != null);
}

class VkErrorState extends VkState {}
