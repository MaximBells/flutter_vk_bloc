import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vk_bloc/bloc/event.dart';
import 'package:vk_bloc/bloc/vk_bloc.dart';

class MainPage extends StatelessWidget {
  String link;
  List<dynamic> loadedUser;

  MainPage(List<dynamic> _loadedUser, String _link) {
    loadedUser = _loadedUser;
    link = _link;
  }

  @override
  Widget build(BuildContext context) {
    final VkBloc vkBloc = BlocProvider.of<VkBloc>(context);
    return Scaffold(
        appBar: AppBar(
          title: Text('Главная',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          backgroundColor: Colors.blue,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(bottom: 30),
            child: Center(
              child: Container(
                  child: Column(children: [
                SizedBox(
                  height: 40.0,
                ),
                Center(
                  child: Stack(
                    alignment: Alignment.bottomRight,
                    children: [
                      CircleAvatar(
                          radius: 90,
                          backgroundImage:
                              NetworkImage(loadedUser[0].photo_200)),
                      loadedUser[0].online_mobile == 1
                          ? Positioned(
                              bottom: 3.0,
                              right: 3.0,
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Stack(children: <Widget>[
                                    Positioned.fill(
                                      child: Container(
                                        margin: EdgeInsets.all(12),
                                        color: Colors.white, // Color
                                      ),
                                    ),
                                    Icon(
                                      Icons.phone_android,
                                      color: Colors.grey[50],
                                      size: 55.0,
                                    ),
                                  ]),
                                  Icon(
                                    Icons.phone_android,
                                    color: Colors.green,
                                    size: 45.0,
                                  ),
                                ],
                              ),
                            )
                          : Positioned(
                              bottom: 5.0,
                              right: 5.0,
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Icon(
                                    Icons.circle,
                                    color: Colors.grey[50],
                                    size: 50.0,
                                  ),
                                  Icon(
                                    Icons.circle,
                                    color: loadedUser[0].online == 1
                                        ? Colors.green
                                        : Colors.grey,
                                    size: 40.0,
                                  ),
                                ],
                              ),
                            ),
                    ],
                  ),
                ),
                Center(
                  child: Text(
                    '${loadedUser[0].first_name} ${loadedUser[0].last_name}',
                    style:
                        TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 80.0,
                ),
                Center(
                    child: Container(
                  width: 200.0,
                  // ignore: deprecated_member_use
                  child: RaisedButton(
                    onPressed: () {
                      vkBloc.add(VkMainPage());
                    },
                    color: Colors.blue,
                    child: Text(
                      'Мой профиль',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                )),
                Center(
                    child: Container(
                  width: 200.0,
                  // ignore: deprecated_member_use
                  child: RaisedButton(
                    onPressed: () {
                      vkBloc.add(VkChatPage(link));
                    },
                    color: Colors.grey[300],
                    child: Text('Мои сообщения'),
                  ),
                )),
                Center(
                    child: Container(
                  width: 200.0,
                  // ignore: deprecated_member_use
                  child: RaisedButton(
                    onPressed: () {
                      vkBloc.add(VkFriendsPage(link));
                    },
                    color: Colors.blue,
                    child: Text(
                      'Мои друзья',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ))
              ])),
            ),
          ),
        ));
  }
}
