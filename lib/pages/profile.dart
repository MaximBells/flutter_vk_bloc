import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vk_bloc/bloc/event.dart';
import 'package:vk_bloc/bloc/vk_bloc.dart';

class Profile extends StatelessWidget {
  List<dynamic> loadedUser;
  String link;

  Profile(List<dynamic> _loadedUser, String _link) {
    loadedUser = _loadedUser;
    link = _link;
  }

  @override
  Widget build(BuildContext context) {
    final VkBloc vkBloc = BlocProvider.of<VkBloc>(context);
    return WillPopScope(
      child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: () {
                vkBloc.add(VkLoadPage(link));
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            title: Text('Личный профиль'),
            backgroundColor: Colors.blue,
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 40),
              child: Column(children: [
                SizedBox(
                  height: 60.0,
                ),
                Center(
                  child: Stack(
                    alignment: Alignment.bottomRight,
                    children: [
                      CircleAvatar(
                          radius: 90,
                          backgroundImage: NetworkImage(loadedUser[0].photo_200)),
                      loadedUser[0].online_mobile == 1
                          ? Positioned(
                              bottom: 3.0,
                              right: 3.0,
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Stack(children: <Widget>[
                                    Positioned.fill(
                                      child: Container(
                                        margin: EdgeInsets.all(12),
                                        color: Colors.white, // Color
                                      ),
                                    ),
                                    Icon(
                                      Icons.phone_android,
                                      color: Colors.grey[50],
                                      size: 55.0,
                                    ),
                                  ]),
                                  Icon(
                                    Icons.phone_android,
                                    color: Colors.green,
                                    size: 45.0,
                                  ),
                                ],
                              ),
                            )
                          : Positioned(
                              bottom: 5.0,
                              right: 5.0,
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Icon(
                                    Icons.circle,
                                    color: Colors.grey[50],
                                    size: 50.0,
                                  ),
                                  Icon(
                                    Icons.circle,
                                    color: loadedUser[0].online == 1
                                        ? Colors.green
                                        : Colors.grey,
                                    size: 40.0,
                                  ),
                                ],
                              ),
                            ),
                    ],
                  ),
                ),
                Center(
                  child: Text(
                    loadedUser[0].first_name,
                    style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
                  ),
                ),
                Center(
                  child: Text(
                    loadedUser[0].last_name,
                    style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
                  ),
                ),
                if (loadedUser[0].status != '')
                  Center(
                    child: Text(
                      "Статус: ${loadedUser[0].status}",
                    ),
                  )
              ]),
            ),
          )),
      onWillPop: () async {
        //Замена события
        vkBloc.add(VkLoadPage(link));
        return false;
      },
    );
  }
}
