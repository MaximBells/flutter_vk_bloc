import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:vk_bloc/model/user.dart';
import 'dart:async';

class UserProvider {
  String access_token = '';
  String user_id = '';

  Future<List<User>> getUser(String link) async {
    var uri = Uri.parse(link);
    access_token = uri.fragment.split('&')[0];
    access_token = access_token.split('=')[1];
    user_id = uri.fragment.split('&')[2];
    user_id = user_id.split('=')[1];
    print(access_token);
    print(user_id);
    var url = Uri.parse(
        'https://api.vk.com/method/users.get?user_id=$user_id&access_token=$access_token&v=5.52&fields=photo_200,online,status');
    var responseAccount = await http.get(url);

    if (responseAccount.statusCode == 200) {
      final List<dynamic> userJson =
          jsonDecode(responseAccount.body)['response'];
      return userJson.map((json) => User.fromJson(json)).toList();
    } else {
      throw Exception('Error fetching users');
    }
  }
}

class MessageProvider {
  String access_token = '';
  String user_id = '';

  Future<List<Message>> getMessage(String link, int peer_id) async {
    var uri = Uri.parse(link);
    access_token = uri.fragment.split('&')[0];
    access_token = access_token.split('=')[1];
    user_id = uri.fragment.split('&')[2];
    user_id = user_id.split('=')[1];
    var url = Uri.parse(
        'https://api.vk.com/method/messages.getHistory?user_id=$user_id&access_token=$access_token&v=5.131&peer_id=$peer_id');
    var mark = Uri.parse(
        'https://api.vk.com/method/messages.markAsRead?access_token=$access_token&v=5.131&peer_id=$peer_id&mark_conversation_as_read=1');
    var markAsRead = await http.get(mark);
    var responseMessage = await http.get(url);
    if (responseMessage.statusCode == 200) {
      final List<dynamic> messageJson =
          jsonDecode(responseMessage.body)['response']['items'];
      return messageJson.map((json) => Message.fromJson(json)).toList();
    } else {
      throw Exception('Error fetching users');
    }
  }

  Future<void> sendMessage(String link, int peer_id, String message) async {
    var uri = Uri.parse(link);
    access_token = uri.fragment.split('&')[0];
    access_token = access_token.split('=')[1];
    user_id = uri.fragment.split('&')[2];
    user_id = user_id.split('=')[1];
    var url = Uri.parse(
        'https://api.vk.com/method/messages.send?peer_id=$peer_id&access_token=$access_token&v=5.45&message=$message&random_id=${Random().nextInt(100)}');
    var responseMessage = await http.get(url);
    print(responseMessage.body);
  }

  Future<List<Message>> getOffsetMessage(
      String link, int peer_id, int offset) async {
    var uri = Uri.parse(link);
    access_token = uri.fragment.split('&')[0];
    access_token = access_token.split('=')[1];
    user_id = uri.fragment.split('&')[2];
    user_id = user_id.split('=')[1];
    var url = Uri.parse(
        'https://api.vk.com/method/messages.getHistory?user_id=$user_id&access_token=$access_token&v=5.131&peer_id=$peer_id&offset=$offset');
    var responseMessage = await http.get(url);
    if (responseMessage.statusCode == 200) {
      final List<dynamic> messageJson =
          jsonDecode(responseMessage.body)['response']['items'];
      return messageJson.map((json) => Message.fromJson(json)).toList();
    } else {
      throw Exception('Error fetching users');
    }
  }
}

class FriendProvider {
  String access_token = '';
  String user_id = '';

  Future<List<Friend>> getFriend(String link) async {
    var uri = Uri.parse(link);
    access_token = uri.fragment.split('&')[0];
    access_token = access_token.split('=')[1];
    user_id = uri.fragment.split('&')[2];
    user_id = user_id.split('=')[1];
    var url = Uri.parse(
        'https://api.vk.com/method/friends.get?user_id=$user_id&access_token=$access_token&v=5.110&fields=nickname,photo_100,photo_200,online,last_seen');
    var responseAccount = await http.get(url);

    if (responseAccount.statusCode == 200) {
      final List<dynamic> friendJson =
          jsonDecode(responseAccount.body)['response']['items'];
      return friendJson.map((json) => Friend.fromJson(json)).toList();
    } else {
      throw Exception('Error fetching friends');
    }
  }
}

class ChatProvider {
  String access_token = '';
  String user_id = '';

  String getName(List<ChatProfiles> chatProfiles, int id) {
    String name = '';
    chatProfiles.forEach((element) {
      if (element.id == id) {
        name = '${element.first_name} ${element.last_name}';
      }
    });
    return name;
  }

  String getPhoto(List<ChatProfiles> chatProfiles, int id) {
    String photo = '';
    chatProfiles.forEach((element) {
      if (element.id == id) {
        photo = element.photo_100;
      }
    });
    return photo;
  }

  Future<List<ChatProfiles>> getChatProfiles(String link) async {
    var uri = Uri.parse(link);
    access_token = uri.fragment.split('&')[0];
    access_token = access_token.split('=')[1];
    user_id = uri.fragment.split('&')[2];
    user_id = user_id.split('=')[1];
    var urlChat = Uri.parse(
        'https://api.vk.com/method/messages.getConversations?&filter=all&access_token=$access_token&v=5.131&extended=1&count=20');
    var responseAccountChat = await http.get(urlChat);

    if (responseAccountChat.statusCode == 200) {
      final List<dynamic> chatJson =
          jsonDecode(responseAccountChat.body)['response']['profiles'];
      return chatJson.map((json) => ChatProfiles.fromJson(json)).toList();
    } else {
      throw Exception('Error fetching users');
    }
  }
}

class ConversationIdProvider {
  String access_token = '';
  String user_id = '';

  Future<List<ConversationsId>> getConversation(String link) async {
    var uri = Uri.parse(link);
    access_token = uri.fragment.split('&')[0];
    access_token = access_token.split('=')[1];
    user_id = uri.fragment.split('&')[2];
    user_id = user_id.split('=')[1];
    var urlChat = Uri.parse(
        'https://api.vk.com/method/messages.getConversations?filter=all&access_token=$access_token&v=5.131&extended=0&count=20');
    var responseAccountChat = await http.get(urlChat);

    if (responseAccountChat.statusCode == 200) {
      final List<dynamic> chatJson =
          jsonDecode(responseAccountChat.body)['response']['items'];
      final List<ConversationsId> conversationJson = [];
      chatJson.forEach((element) {
        if (element['conversation']['peer']['type'] == 'user') {
          conversationJson.add(ConversationsId(
              element['conversation']['peer']['type'],
              element['conversation']['peer']['id'],
              '',
              '',
              element['last_message']['text'],
              element['last_message']['date'],
              element['conversation']['unread_count'],
              element['conversation']['in_read'],
              element['conversation']['out_read']));
        }
        if (element['conversation']['peer']['type'] == 'chat' &&
            element['conversation']['chat_settings']['photo'] != null) {
          conversationJson.add(ConversationsId(
              element['conversation']['peer']['type'],
              element['conversation']['peer']['id'],
              element['conversation']['chat_settings']['photo']['photo_100'],
              element['conversation']['chat_settings']['title'],
              element['last_message']['text'],
              element['last_message']['date'],
              element['conversation']['unread_count'],
              element['conversation']['in_read'],
              element['conversation']['out_read']));
        }
        if (element['conversation']['peer']['type'] == 'chat' &&
            element['conversation']['chat_settings']['photo'] == null) {
          conversationJson.add(ConversationsId(
              element['conversation']['peer']['type'],
              element['conversation']['peer']['id'],
              'https://drasler.ru/wp-content/uploads/2019/05/Рисунки-по-клеткам-для-профи-016-300x300.jpg',
              element['conversation']['chat_settings']['title'],
              element['last_message']['text'],
              element['last_message']['date'],
              element['conversation']['unread_count'],
              element['conversation']['in_read'],
              element['conversation']['out_read']));
        }
      });
      return conversationJson;
    } else {
      throw Exception('Error fetching users');
    }
  }
}
