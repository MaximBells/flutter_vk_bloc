import 'package:encrypt/encrypt.dart' as Enc;
import 'dart:convert';
import 'package:string_unescape/string_unescape.dart';
import 'package:crypto/crypto.dart';

class Crypt {
  String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }
}

class AesCrypt extends Crypt {
  Future<String> encrypt(
      String keyStr, String plainText, String typeOfCrypt) async {
    String _enc;

    keyStr = generateMd5(keyStr);
    final key = Enc.Key.fromUtf8(keyStr);
    final iv = Enc.IV.fromLength(16);
    final encrypter = Enc.Encrypter(Enc.AES(key));
    final encrypted = encrypter.encrypt(plainText, iv: iv);
    _enc = encrypted.base64;
    _enc = 'messageCrypt\$$typeOfCrypt\$${base64.normalize(_enc)}';
    return _enc;
  }

  Future<String> decrypt(String keyStr, String plainText) async {
    String _dec;

    keyStr = generateMd5(keyStr);

    final key = Enc.Key.fromUtf8(keyStr);
    final iv = Enc.IV.fromLength(16);
    final encrypter = Enc.Encrypter(Enc.AES(key));

    final encryptedText = Enc.Encrypted.fromBase64(unescape(plainText));
    try {
      final decrypted = encrypter.decrypt(encryptedText, iv: iv);
      _dec = decrypted;
    } catch (error) {
      _dec = 'Ошибка. Неправильный ключ, попробуйте другой.';
    }
    return _dec;
  }
}

class Salsa20Crypt extends Crypt {
  Future<String> encrypt(
      String keyStr, String plainText, String typeOfCrypt) async {
    String _enc;

    keyStr = generateMd5(keyStr);


    final key = Enc.Key.fromUtf8(keyStr);
    final iv = Enc.IV.fromLength(8);
    final encrypter = Enc.Encrypter(Enc.Salsa20(key));
    final encrypted = encrypter.encrypt(plainText, iv: iv);
    _enc = encrypted.base64;
    _enc = 'messageCrypt\$$typeOfCrypt\$${base64.normalize(_enc)}';
    return _enc;
  }

  Future<String> decrypt(String keyStr, String plainText) async {
    String _dec;

    keyStr = generateMd5(keyStr);

    final key = Enc.Key.fromUtf8(keyStr);
    final iv = Enc.IV.fromLength(8);
    final encrypter = Enc.Encrypter(Enc.Salsa20(key));

    final encryptedText = Enc.Encrypted.fromBase64(unescape(plainText));
    final decrypted = encrypter.decrypt(encryptedText, iv: iv);

    _dec = decrypted;
    return _dec;
  }
}

class FernetCrypt extends Crypt {
  Future<String> encrypt(
      String keyStr, String plainText, String typeOfCrypt) async {
    String _enc;

    keyStr = generateMd5(keyStr);
    keyStr = keyStr.substring(0, 24);
    final key = Enc.Key.fromUtf8(base64Url.encode(keyStr.codeUnits));
    //final b64key = Enc.Key.fromUtf8(base64Url.encode(key.bytes));
    final iv = Enc.IV.fromLength(16);
    final encrypter = Enc.Encrypter(Enc.Fernet(key));
    final encrypted = encrypter.encrypt(plainText, iv: iv);
    _enc = encrypted.base64;
    _enc = 'messageCrypt\$$typeOfCrypt\$${base64.normalize(_enc)}';
    return _enc;
  }

  Future<String> decrypt(String keyStr, String plainText) async {
    String _dec;

    keyStr = generateMd5(keyStr);
    keyStr = keyStr.substring(0, 24);

    final key = Enc.Key.fromUtf8(base64Url.encode(keyStr.codeUnits));
    final encrypter = Enc.Encrypter(Enc.Fernet(key));

    final encryptedText = Enc.Encrypted.fromBase64(unescape(plainText));
    try {
      final decrypted = encrypter.decrypt(encryptedText);
      _dec = decrypted;
    } catch (error) {
      _dec = 'Ошибка. Неправильный ключ, попробуйте другой.';
    }
    return _dec;
  }
}
